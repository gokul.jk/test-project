var QAList = [];
$(document).ready(function () {
  localStorage.setItem("counter", 2);
  $("#addSection").on("click", function (e) {
    CreateHTMLDynamically();
    // var frm = $(".frm:first").clone();
    // frm.find("input").val("");
    // $(".frm:last").after(frm);
  });
  $(window).bind("beforeunload", function () {
    //save info somewhere

    localStorage.setItem("counter", 1);
  });
  $("#generate").click(() => {
    SaveQuestionAndAnswers();
  });
});
function getAllQA() {
  return QAList;
}
function SaveQuestionAndAnswers() {
  let counter = 1;
  let redirect = true;
  let sectionLength = localStorage.getItem("counter");
  for (let i = 0; i < sectionLength - 1; i++) {
    let ques = $("#questionTxt" + counter).val();
    let ans = $("#answerInputTxt" + counter).val();
    let datePicValue = $("#datePickerIDTxt" + counter).val();
    let defaultAns = $("#defaultAnswerTxt" + counter).val();
    let score = $("#scoreTxt" + counter).val();
    if (ques == "" || ans == "" || defaultAns == "" || score == "") {
      alert("Please enter all mandatory fields!!");
      $("#ErrorMsg").show();
      redirect = false;
    } else {
      let singleQA = {
        question: ques,
        answer: ans,
        datePicerValue: datePicValue,
        dafaultAnswer: defaultAns,
        score: score,
      };
      QAList.push(singleQA);
      counter++;
    }
  }
  localStorage.setItem("QALists", JSON.stringify(QAList));
  if (redirect) {
    localStorage.setItem("counter", 1);
    window.location.href = "student.html";
  }
}
function printMsg() {
  alert("Called!!");
}
// function startTimer() {
//   var h6 = document.getElementsByTagName("h6");
//   h6[0].innerHTML = "Timer";

//   var sec = 300,
//     countDiv = document.getElementById("timer"),
//     secpass,
//     countDown = setInterval(function () {
//       "use strict";

//       secpass();
//     }, 1000);

//   function secpass() {
//     "use strict";

//     var min = Math.floor(sec / 60),
//       remSec = sec % 60;

//     if (remSec < 10) {
//       remSec = "0" + remSec;
//     }
//     if (min < 10) {
//       min = "0" + min;
//     }
//     countDiv.innerHTML = min + ":" + remSec;

//     if (sec > 0) {
//       sec = sec - 1;
//     } else {
//       clearInterval(countDown);

//       countDiv.innerHTML = "countdown done";
//     }
//   }
// }

function CreateHTMLDynamically() {
  let counter = localStorage.getItem("counter");
  if (counter == null) {
    counter = 2;
  }

  var newTextBoxDiv = $(document.createElement("div")).attr(
    "id",
    "TextBoxDiv" + counter
  );

  newTextBoxDiv.after().html(
    "<label id='label" +
      counter +
      "'>Question " +
      counter +
      " : </label>" +
      '<input class="form-control" type="text" name="textbox' +
      counter +
      '" id="questionTxt' +
      counter +
      '" value="" >' +
      "<br>" +
      "<div class='type'>" +
      "<label id=label" +
      counter +
      ">Answer-type</label><select class='form-control' id='answerType" +
      counter +
      "' name='answer-type'><option value='text'>text</option><option value='option'>option</option><option value='date'>date</option></select></div>" +
      "<br>" +
      "<label id='label" +
      counter +
      "'>Answer " +
      counter +
      " : </label>" +
      '<input class="form-control" type="text" name="textbox' +
      counter +
      '" id="answerInputTxt' +
      counter +
      '" value="" >' +
      "<br>" +
      "<label id='label" +
      counter +
      "'>Default answer " +
      counter +
      " : </label>" +
      '<input class="form-control" type="text" name="textbox' +
      counter +
      '" id="defaultAnswerTxt' +
      counter +
      '" value="" >' +
      "<br>" +
      "<label id='label" +
      counter +
      "'>Score " +
      counter +
      " : </label>" +
      '<input class="form-control" type="text" name="textbox' +
      counter +
      '" id="scoreTxt' +
      counter +
      '" value="" >' +
      "<hr>"
    // +
    // "<br>" +
    // "<label id='label" +
    // counter +
    // "'>Question " +
    // counter +
    // "<div class='type'>"+
    // "<label id='label" +
    // counter +
    // "'>Question " +
    // counter +
    // " : </label>" +
    // '<input type="text" name="textbox' +
    // counter +
    // '" id="questionTxt' +
    // counter +
    // '" value="" >'
  );

  newTextBoxDiv.appendTo(".frm");

  counter++;
  localStorage.setItem("counter", counter);
}
